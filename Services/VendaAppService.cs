using tech_test_payment_api.Application.Enums;
using tech_test_payment_api.Infra.Data.Entities;
using tech_test_payment_api.Infra.Data.Entities.Vendas;
using tech_test_payment_api.Infra.Data.Repositories.Interfaces;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Services;

public class VendaAppService : IVendaAppService
{
    private readonly IVendaRepository _vendaRepository;
    private readonly IItemRepository _itemRepository;

    public VendaAppService(IVendaRepository vendaRepository, IItemRepository itemRepository)
    {
        _vendaRepository = vendaRepository;
        _itemRepository = itemRepository;
    }

    public Venda InserirVenda(VendaResumida venda)
    {
        if (venda.Itens.Count() == 0 || venda.VendedorId <= 0)
        {
            var vendaResponse = new Venda();
            vendaResponse = null;
            return vendaResponse;
        }

        var itensList = new List<Venda_Item>();

        foreach (var item in venda.Itens)
        {
            int itemId = _itemRepository.ObterIdByNome(item.Nome);
            itensList.Add(new Venda_Item(itemId));
        }

        IEnumerable<Venda_Item> listaVendaItem = itensList;

        var newVenda = new Venda(venda.VendedorId);
        newVenda.Itens = listaVendaItem;
        var vendaAdicionada = _vendaRepository.Inserir(newVenda);

        return vendaAdicionada;
    }

    public Venda BuscarVendaPorId(int id)
    {
        return _vendaRepository.BuscarPorId(id);
    }
    public Venda AtualizarVenda(int id, EStatusVenda status)
    {
        var venda = BuscarVendaPorId(id);
        var vendaVerificada = VerificaStatusValido(venda, status);
        if (vendaVerificada == null)
        {
            return vendaVerificada;
        }
        return _vendaRepository.Atualizar(id, status);
        
    }

    public Venda VerificaStatusValido(Venda venda, EStatusVenda status)
    {
        bool aprovadaOuCancelada = status == EStatusVenda.PagamentoAprovado || status == EStatusVenda.Cancelada;
        if (venda.Status == EStatusVenda.AguardandoPagamento && !aprovadaOuCancelada)
        {
            venda = null;
            return venda;
        }

        bool enviadaOuCancelada = status == EStatusVenda.EnviadoParaTransportadora || status == EStatusVenda.Cancelada;
        if (venda.Status == EStatusVenda.PagamentoAprovado && !enviadaOuCancelada)
        {
            venda = null;
            return venda;
        }

        if (venda.Status == EStatusVenda.EnviadoParaTransportadora && status != EStatusVenda.Entregue)
        {
            venda = null;
            return venda;
        }

        if (venda.Status == EStatusVenda.Cancelada)
        {
            venda = null;
            return venda;
        }

        return venda;
    }
}