using tech_test_payment_api.Application.Enums;
using tech_test_payment_api.Infra.Data.Entities.Vendas;

namespace tech_test_payment_api.Services.Interfaces;

public interface IVendaAppService
{
    Venda InserirVenda(VendaResumida venda);
    Venda BuscarVendaPorId(int id);
    Venda AtualizarVenda(int id, EStatusVenda status);
}