using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Application.Enums;
using tech_test_payment_api.Infra.Data.Entities.Vendas;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Controllers;
[ApiController]
[Route("api/vendas")]
public class VendaController : ControllerBase
{
    private readonly IVendaAppService _vendaAppService;

    public VendaController(IVendaAppService vendaAppService)
    {
        _vendaAppService = vendaAppService;
    }

    [HttpPost("add")]
    public IActionResult CadastrarVenda(VendaResumida venda)
    {
        var vendaAdicionada = _vendaAppService.InserirVenda(venda);
        if (vendaAdicionada == null)
        {
            return BadRequest(new { Erro = "N�o existem itens na venda ou o Id do vendedor n�o foi informado!!" });
        }
        return CreatedAtAction(nameof(BuscarVendaPorId), new { id = vendaAdicionada.IdPedido }, vendaAdicionada);
    }

    [HttpGet("{id}")]
    public IActionResult BuscarVendaPorId(int id)
    {
        var venda = _vendaAppService.BuscarVendaPorId(id);
        if (venda == null)
        {
            return NotFound();
        }
        return Ok(venda);
    }

    [HttpPut("atualizar/{id}")]
    public IActionResult AtualizarVenda(int id, [FromQuery] EStatusVenda status)
    {
        if (id == 0)
        {
            return NotFound();
        }

        var venda = _vendaAppService.AtualizarVenda(id, status);
        if (venda == null)
        {
            return BadRequest(new { Erro = "Altera��o de Status Inv�lida!!" });
        }
        return Ok(venda);
    }
}
